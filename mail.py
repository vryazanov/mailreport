__author__ = 'vryazanov'
__all__ = ['send_mail']

import smtplib, os
from email.mime.multipart import MIMEMultipart
from email.mime.base import MIMEBase
from email.mime.text import MIMEText
from email.utils import COMMASPACE, formatdate
from email import encoders
import settings

def send_mail(send_from, send_to, subject, text,
              files=[], server=settings.MAIL_SMTP_SERVER, port=settings.MAIL_SMTP_PORT,
              username=settings.MAIL_LOGIN, password=settings.MAIL_PASSWORD, isTls=True):
    msg = MIMEMultipart()
    msg['From'] = send_from
    msg['To'] = COMMASPACE.join(send_to)
    msg['Date'] = formatdate(localtime = True)
    msg['Subject'] = subject

    msg.attach( MIMEText(text) )

    for f in files:
        part = MIMEBase('application', "octet-stream")
        part.set_payload( open(f,"rb").read() )
        encoders.encode_base64(part)
        part.add_header('Content-Disposition', 'attachment; filename="{0}"'.format(os.path.basename(f)))
        msg.attach(part)

    smtp = smtplib.SMTP(server, port)
    if isTls: smtp.starttls()
    smtp.login(username,password)
    smtp.sendmail(send_from, send_to, msg.as_string())
    smtp.quit()

if __name__ == '__main__':
    send_mail('smev-support@yandex.ru', ['vryazanov@at-consulting.ru'], 'test', 'test2', ['D:\\BitBucket\\report.py'])
