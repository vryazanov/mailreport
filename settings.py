"""
Настройки скрипта.
"""
from datetime import datetime, timedelta

SEASON = 7 #Период выгрузки отчетов (в днях)
MAIL_LOGIN = 'smev-support'
MAIL_PASSWORD = 'smevsupport'
MAIL_SMTP_SERVER = 'smtp.yandex.ru'
MAIL_SMTP_PORT = 587


MAILS = {
    'Федеральная служба безопасности России (ФСБ России)':
        ['smev@fsb.ru', 'StarodubovAG@infotecs.ru', 'KykoAS@infotecs.ru', 'mva.com@mail.ru'],
    'Федеральная служба по надзору в сфере транспорта (Ространснадзор)':
        ['smev@rostransnadzor.ru', 'Tkachenko_AI@rostransnadzor.ru', 'Baklykov_AY@rostransnadzor.ru', 'Gulin_VB@rostransnadzor.ru', 'Shestopalov_YM@rostransnadzor.ru', 'Burtovoy_VP@rostransnadzor.ru', 'Stepanov_IV@rostransnadzor.ru'],
    'Управление регистрации и архивных фондов Федеральной службы безопасности Российской Федерации':
        ['smev@fsb.ru'],
    'Министерство внутренних дел Российской Федерации (МВД)':
        ['smev@mvd.ru', 'lesnikov@gibdd.ru', 'Nickushka@mail.ru', 'sgozalov@mail.ru', 'a109147@mail.ru', '6676540@gmail.com', 'dit@mvd.ru'],
    'Федеральный фонд обязательного медицинского страхования (ФОМС)':
        ['smev@ffoms.ru'],
    'Пенсионный фонд Российской Федерации (ПФР)':
        ['support@101.pfr.ru', 'smev@100.pfr.ru', 'epetina@100.pfr.ru', 'kyankin@100.pfr.ru', 'kuleshov@100.pfr.ru'],
    'Министерство обороны РФ':
        ['smev@mil.ru', 'modem2006@mail.ru', 'modem2006@mail.ru'],
    'Федеральный институт промышленной собственности (ФИПС)':
        ['smev@rupto.ru', 'DKravcov@rupto.ru'],
    'Федеральное агентство по недропользованию (РОСНЕДРА)':
        ['210fz@rosnedra.com', 'dzamyatina@rosnedra.com', 'smev@rosnedra.com', 'dpyatakov@rosnedra.com'],
    'Министерство юстиции Российской Федерации (Минюст)':
        ['smev@minjust.ru', 'bokov_du@minjust.ru', 'smev@minjust.ru', 'nberezhok@scli.ru', 'root@scli.ru', 'minjust_smev@mail.ru'],
    'Федеральное агентство по водным ресурсам (Росводресурсы)':
        ['smev@favr.ru', 'gorunov_s@favr.ru.', 'zub_m@favr.ru', 'dunia@inbox.ru', 'zlobina@favr.ru', 'o_romanova@favr.ru', 'c_becherikova@favr.ru', 'oka@favr.ru', 'gorunov_s@favr.ru', 'chehonin@favr.ru'],
    'Федеральная служба по финансовому мониторингу (Росфинмониторинг)':
        ['eis2@fedsfm.ru', 'eis2@fedsfm.ru', 'eis2@fedsfm.ru'],
    'Федеральная служба по регулированию алкогольного рынка (Росалкогольрегулирование)':
        ['smev@fsrar.ru', 'kuzmin-ip@fsrar.ru', 'karpov-am@fsrar.ru', 'mikhalev-au@fsrar.ru'],
    'Федеральное агентство по рыболовству  (Росрыболовство)':
        ['smev@fishcom.ru', 'kopytov@fishcom.ru', 'fursova@fishcom.ru', 'shymanski@fishcom.ru', 'yuris@kck.ru', 'fomin@fishcom.ru'],
    'Федеральная антимонопольная служба (ФАС России)':
        ['smev@fas.gov.ru', 'fedorov@fas.gov.ru', 'lmal@fas.gov.ru', 'lim@fas.gov.ru'],
    'Министерство здравоохранения Российской Федерации':
        ['smev@rosminzdrav.ru'],
    'Министерство культуры Российской Федерации (Минкультуры)':
        ['smev@mkrf.ru', 'zubov@mkrf.ru', 'sviivanov@gmail.com', 'r.frolov@bssys.com'],
    'Министерство иностранных дел Российской Федерации (МИД)':
        ['smev@mid.ru', 'mishin@fgosniias.ru', 'lponomarenko@mid.ru, igrkorn@mail.ru', 'dio@mid.ru'],
    'Федеральная служба по надзору в сфере связи, информационных технологий и массовых коммуникаций (Роскомнадзор)':
        ['smev@rsoc.ru', 'N.Valeev@rsoc.ru', 'a.nekludov@e-soft.ru', 'D.Tarasov@rsoc.ru'],
    'Федеральная служба по надзору в сфере защиты прав потребителей и благополучия человека (Роспотребнадзор)':
        ['smev@gsen.ru', 'Tebiev_SA@gsen.ru', 'Gleb@crc.ru'],
    'Федеральная служба государственной статистики (Росстат)':
        ['smev@gks.ru', 'belob_a@gks.ru'],
    'Федеральная служба по надзору в сфере здравоохранения (Росздравнадзор)':
        ['smev@roszdravnadzor.ru', 'pospekovKG@roszdravnadzor.ru', 'kostenkoms@roszdravnadzor.ru', 'chevgaevaag@roszdravnadzor.ru'],
    'Федеральная служба по экологическому, технологическому и атомному надзору (Ростехнадзор)':
        ['smev@gosnadzor.ru', 'k.mihalkin@gosnadzor.ru', 'a.chaykina@gosnadzor.ru', 'ozp@gosnadzor.ru'],
    'Федеральная служба судебных приставов (ФССП)':
        ['fssp.smev@red-soft.biz'],
        #['smev@fssprus.ru', 'fssp.smev@red-soft.biz'],
    'Федеральное агентство по печати и массовым коммуникациям (Роспечать)':
        ['smev@fapmc.ru', 'tsafanasenko@fapmc.ru', 'aaseregin@fapmc.ru', 'lizin@gmail.com'],
    'Федеральное агентство связи (Россвязь)':
        ['opss@minsvyaz.ru', 'otdnum@minsvyaz.ru', 'nikolay.skvortsov@red-soft.biz'],
    'Федеральное дорожное агентство Министерства транспорта Российской Федерации':
        ['smev@rdis.fad.ru', 'dmitrenkoea@fad.ru', 'lizyaev@rdis.fad.ru', 'chausov@rdis.fad.ru', 'drushic@rdis.fad.ru', 'sivkopp@gucmp.ru'],
    'Фонд социального страхования РФ (ФСС)':
        ['smev@fss.ru', 'v.sychugina@fss.ru', 'f.moryakov@fss.ru', 'Aleksandr.Tolstousov@sbsoft.ru', 'a.shadrina@fss.ru'],
    'Федеральная служба исполнения наказаний (ФСИН России)':
        ['smev@fsin.su'],
    'Федеральное государственное унитарное предприятие':
        ['smev@russianpost.ru'],
    'Министерство Российской Федерации по делам гражданской обороны, чрезвычайным ситуациям и ликвидации последствий стихийных бедствий (МЧС)':
        ['smev@mchs.gov.ru'],
    'Федеральное агентство морского и речного транспорта (Росморречфлот)':
        ['smev@morflot.ru', 'abornevvs@morflot.ru', 'nechaevvp@morflot.ru'],
    'Министерство промышленности и торговли Российской Федерации (Минпромторг России)':
        ['smev@minprom.gov.ru', 'Bokov@minprom.gov.ru', 'kasimova@systemprojects.ru', 'K.zarenkov@sibcon-soft.ru'],
    'Федеральная таможенная служба (ФТС)':
        ['smev@ca.customs.ru', 'KryukovIM@ca.customs.ru', 'videevkv@ca.customs.ru', 'DemchenkoAA@ca.customs.ru'],
    'Министерство финансов Российской Федерации (Минфин России)':
        ['smev@minfin.ru'],
    'Федеральное агентство по управлению государственным имуществом (Росимущество)':
        ['smev@rosim.ru', 'Raspopov_v_v@rosim.ru', 'Chuykova.IV@rosim.ru', 'v.tsaregorodtseva@rosim.ru'],
    'Федеральное казначейство (ФК)':
        ['smev@roskazna.ru'],
    'Федеральная служба по надзору в сфере природопользования (Росприроднадзор)':
        ['smev@rpn.gov.ru', 'ada@rpn.gov.ru', 'npresnov@adicom.ru'],
    'Федеральная служба по надзору в сфере образования и науки (Рособрнадзор)':
        ['smev@obrnadzor.gov.ru'],
    'Федеральное агентство по техническому регулированию и метрологии (Росстандарт)':
        ['smev@gost.ru', 'iefanova@gost.ru', 'SID0003234@gost.ru', 'SID0003239@gost.ru', 'SID0003265@gost.ru', 'SID0003266@gost.ru'],
    'Министерство сельского хозяйства Российской Федерации (Минсельхоз России)':
        ['smev@econ.mcx.ru', 'a.homyakov@gvc.mcx.ru', 'm.pavlyuchenko@gvc.mcx.ru'],
    'Главное управление государственной экспертизы (Главгосэкспертиза России)':
        ['smev@gge.ru'],
    'Министерство связи и массовых коммуникаций (Минкомсвязь)':
        ['smev@minsvyaz.ru', 'a.korzhenevskaya@minsvyaz.ru'],
    'Министерство энергетики Российской Федерации    (Минэнерго России)':
        ['smev@minenergo.gov.ru', 'ShilinaMN@minenergo.gov.ru', 'NovikovEV@minenergo.gov.ru', 'damir@energy.gov.ru', 'PavlovaNA@minenergo.gov.ru'],
    'Министерство экономического развития РФ (Минэкономразвития)':
        ['smev@economy.gov.ru', 'grinenko@economy.gov.ru', 'vankaev@economy.gov.ru', 'silaevata@economy.gov.ru'],
    'Федеральное агентство железнодорожного транспорта  (Росжелдор)':
        ['smev@roszeldor.ru', 'kopaev@kck.ru', 'a.bespalov@roszeldor.ru'],
    'Федеральная служба по гидрометеорологии и мониторингу окружающей среды (Росгидромет)':
        ['smev@hmuslugi.ru', 'gusev@mecom.ru', 'korot@mcc.mecom.ru'],
    'Федеральная служба по техническому и экспортному контролю':
        ['smev@fstec.ru', 'smev@fstec.ru', 'smev@fstec.ru', 'smev@fstec.ru', 'exportcon2@fstec.ru'],
    'Министерство образования и науки (Минобрнауки России)':
        ['smev@mon.gov.ru', 'barabash@mon.gov.ru', 'romanov@mon.gov.ru', 'landar@informika.ru'],
    'Федеральное агентство воздушного транспорта (Росавиация)':
        ['smev@scaa.ru', 'Igonin_OG@scaa.ru', 'Matveev_KA@scaa.ru', 'a.meleshkov@shuffle.ru'],
    'Министерство труда и социального развития Российской Федерации':
        ['smev@rosmintrud.ru'],
    'Федеральное агентство по туризму (Ростуризм)':
        ['smev@russiatourism.ru', 'smev@russiatourism.ru'],
    'Федеральная служба по оборонному заказу    (Рособоронзаказ)':
        ['smev@fsoz.gov.ru', 'palatov@fsoz.gov.ru', 'fedorenko@fsoz.gov.ru', 'a.koptev@shuffle.ru'],
    'Федеральная служба по ветеринарному и фитосанитарному надзору (Россельхознадзор)':
        ['smev@fsvps.ru', 'a.staroskolsky@gmail.com'],
    'Федеральная служба государственной регистрации кадастра и картографии (Росреестр)':
        ['smev@rosreestr.ru', 'Alexey.Makarov@fccland.ru'],
    'Министерство регионального развития РФ (Минрегион)':
        ['smev@minregion.ru', 'igor.stratan@minregion.ru', 'olga.tsareva@minregion.ru', 'Elena.Kuznetsova@minregion.ru', 'Roman.Sergushko@minregion.ru', 'S.Churbakov@minregion.ru'],
    'Федеральная служба по финансовым рынкам (ФСФР)':
        ['smev@ffms.ru'],
    'Федеральная служба по аккредитации (Росаккредитация)':
        ['smev@fsa.gov.ru,', 'fsa-it@fsa.gov.ru;', 'fsa-it@fsa.gov.ru;'],
    'Федеральная служба охраны Российской Федерации':
        ['smev.fso@gov.ru', 'zalozniy_su@gov.ru', 'zalozniy_su@gov.ru', 'zalozniy_su@gov.ru'],
    'Федеральное медико-биологическое агентство (ФМБА)':
        ['smev@fmbamail.ru', 'pt@fmbamail.ru', 'kolbutov@nic-itep.ru', 'popova@nic-itep.ru', 'ovz@nic-itep.ru'],
    'Федеральное бюджетное учреждение "Государственная регистрационная палата при Министерстве юстиции Российской Федерации"  (ФБУ ГРП при Минюсте России)':
        ['smev@palata.ru', 'gm_umerenkov@palata.ru', 'ai_sobolev@palata.ru', 'ua_yashina@palata.ru', 'pa_gaponov@palata.ru'],
    'Федеральное космическое агенство (Роскосмос)':
        ['b_ignatov@roscosmos.ru'],
    'Министерство экономического развития Российской Федерации':
        ['smev@economy.gov.ru', 'grinenko@economy.gov.ru', 'vankaev@economy.gov.ru', 'silaevata@economy.gov.ru']
}

TO_DATE = datetime.today()
FROM_DATE = TO_DATE - timedelta(SEASON)

if __name__ == '__main__':
    print(len(MAILS))