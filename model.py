"""
Классы отчетов должны наследоваться от Model.
"""

__author__ = 'vryazanov'
__all__ = ['InaccessibilityModel', 'IncomingRequestModel', 'OutboxRequestModel', 'IncidentPending', 'OIVIncident']

from abc import ABCMeta, abstractmethod
from composite import Component
from itertools import groupby
from algorithms import *
from database import DataFactory
from excel import Writer
import settings
from datetime import datetime, timedelta
import logging

class Model(metaclass=ABCMeta):
    """
    Абстрактный класс отчетов.
    """
    def __init__(self):
        self.obj = Writer()
        self.organization = None
        self.log = logging.getLogger('main')

    @abstractmethod
    def execute(self):
        pass

    def generate(self, organization):
        assert isinstance(organization, Component)
        self.organization = organization
        self.execute()
        self.log.info('Вкладка <{}> для <{}> сформирована.'.format(self.name, self.organization.get_mnemonic()))

#-Заявки и инциденты  ФОИВ----------------------
class OIVIncident(Model):
    def execute(self):
        self.name = 'Заявки и инциденты  ФОИВ'
        self.obj.create_sheet(self.name)
        self.obj.add_header('ID инцидента   ', 'Дата создания    ', 'Дата предоставления полной информации', 'Тема заявки', 'Тип запроса', 'Статус' , 'Кол-во дней обработки')
        data = DataFactory()
        org_mail_list = settings.MAILS[self.organization.get_name()][:]
        org_mail_list.append('None')
        org_mail_list.append('None')
        #Номер инциденты, Дата создания, Тема заявки, Тип запроса, Дата перехода в текущий статус, Последний комментарий, Статус
        db_data = data.get_data('incidentOIV', format={'mails': str(tuple(org_mail_list)), 'status': "('In Progress', 'Assigned')"})
        db_fullInformation = data.get_data('completeInformation', format={'mails': str(tuple(org_mail_list))})
        tickets = {x[0]: [x[1], x[2], x[3], x[4], x[5], x[6]] for x in db_data}
        fullInformation= {}
        for g in groupby(db_fullInformation ,key=lambda x:x[0]):
            fullInformation[g[0]] = get_info([[x[3], x[4], x[-1]] for x in g[1]])

        for incident in tickets:
            self.obj.add_line(incident,
                              '{:%d.%m.%Y %H:%M:%S}'.format(tickets[incident][0]),
                              '{:%d.%m.%Y %H:%M:%S}'.format(fullInformation[incident][0] if fullInformation[incident][0] else tickets[incident][3]),
                              tickets[incident][1],
                              tickets[incident][2],
                              tickets[incident][-1],
                              sec_to_workday(worktime(fullInformation[incident][0] if fullInformation[incident][0] else tickets[incident][3], datetime.today())))
 #-----------------------------------------------

#-Зависшие заявки и  инциденты на стороне ФОИВ--
class IncidentPending(Model):
    def execute(self):
        self.name = 'Заявки на стороне ФОИВ'
        self.obj.create_sheet(self.name)
        self.obj.add_header('ID инцидента   ', 'Дата создания    ', 'Тема заявки', 'Тип запроса', 'Дата перехода в текущий статус' , 'Последний комментарий, причина', 'Кол-во дней в ожидании')
        data = DataFactory()
        org_mail_list = settings.MAILS[self.organization.get_name()][:]
        org_mail_list.append('None')
        org_mail_list.append('None')
        #Номер инциденты, Дата создания, Тема заявки, Тип запроса, Дата перехода в текущий статус, Последний комментарий
        db_data = data.get_data('incidentOIV', format={'mails': str(tuple(org_mail_list)), 'status': "('Pending')"})
        tickets = {x[0]: [x[1], x[2], x[3], x[4], x[5].read()] for x in db_data}
        for incident in tickets:
            self.obj.add_line(incident, '{:%d.%m.%Y %H:%M:%S}'.format(tickets[incident][0]),
                              tickets[incident][1], tickets[incident][2], '{:%d.%m.%Y %H:%M:%S}'.format(tickets[incident][3]),
                              tickets[incident][4], sec_to_workday(worktime(tickets[incident][3], datetime.today())))
#-----------------------------------------------

#--------------Кол-во обращений от ФОИВ---------
class OutboxRequestModel(Model):
    def execute(self):
        self.name = 'Кол-во обращений от ФОИВ'
        self.obj.create_sheet(self.name)
        self.obj.add_header('Владелец сервиса', 'Наименование сервиса', 'SID сервиса', 'Кол-во обращений')
        for information_system in self.organization:
            data = DataFactory()
            requests = data.get_data('outboxRequests', format={'from_date': '{:%d.%m.%Y}'.format(settings.FROM_DATE), 'to_date': '{:%d.%m.%Y}'.format(settings.TO_DATE), 'mnemonic': information_system.get_mnemonic()})
            for org_name, srv_name, service_sid, count in requests:
                self.obj.add_line(org_name, srv_name, service_sid, count)
#----------------------------------------------


#--------------Кол-во обращений к ФОИВ---------
class IncomingRequestModel(Model):
    def execute(self):
        self.name = 'Кол-во обращений к ФОИВ'
        self.obj.create_sheet(self.name)
        self.obj.add_header('Потребитель сервиса', 'Наименование сервиса', 'SID сервиса', 'Кол-во обращений')
        for information_system in self.organization:
            for service in information_system:
                data = DataFactory()
                count = data.get_data('incomingRequests', format={'from_date': '{:%d.%m.%Y}'.format(settings.FROM_DATE), 'to_date': '{:%d.%m.%Y}'.format(settings.TO_DATE), 'sid': service.get_sid()})[0][0]
                self.obj.add_line(self.organization.get_name(), service.get_name(), service.get_sid(), str(count) if count else '0')
#----------------------------------------------


#--------------Отчет по недоступности----------
class InaccessibilityModel(Model):
    def execute(self):
        self.name = 'Недоступность сервисов'
        self.obj.create_sheet(self.name)
        self.obj.add_header('Участник', 'Информационная система', 'SID сервиса', 'Наименование сервиса', 'Суммарное время недоступности')
        for information_system in self.organization:
            for service in information_system:
                data = DataFactory()
                time = inaccessibility_alg(
                    data.get_data('inaccessibilityService', format={'from_date': '{:%d.%m.%Y}'.format(settings.FROM_DATE), 'to_date': '{:%d.%m.%Y}'.format(settings.TO_DATE), 'sid': service.get_sid()})[:],
                    datetime.combine(settings.FROM_DATE, datetime.min.time()),
                    datetime.combine(settings.TO_DATE, datetime.min.time()))
                self.obj.add_line(self.organization.get_name(), information_system.get_name(), service.get_sid(), service.get_name(), time)
#----------------------------------------------

#---------------Тесты--------------------------
class ModelTestDrive(Model):
    def execute(self):
        print(self.organization)
        self.obj.create_sheet('ModelTestDrive')
        self.obj.add_header('1', '2', '3')

class ModelTestDrive1(Model):
    def execute(self):
        print(self.organization)
        self.obj.create_sheet('ModelTestDrive1')
        self.obj.add_header('4', '5', '6')
#----------------------------------------------

if __name__ == '__main__':
    models = [ModelTestDrive, ModelTestDrive1]
    organizations = [Component('FOIV1','FOIV1'), Component('FOIV2','FOIV2')]

    w = Writer()
    for org in organizations:
        w.create_book(org.get_name())
        for model in models:
            m = model()
            m.generate(org)
        w.save_book()





