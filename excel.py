"""
Представлен класс обертка над модулем работы с Excel таблицами.
"""

__author__ = 'vryazanov'
__all__ = ['Writer']

import xlwt3
import os
from datetime import datetime

def get_width(num_characters):
    return int((1 + len(num_characters)) * 256)

class Writer:
    """
    Основной класс формирования файлов с отчетами.
    Класс является синглтоном.
    """
    def __new__(cls):
        if not hasattr(cls, 'instance'):
            cls.instance = super(Writer, cls).__new__(cls)
            cls._file_name = None
            cls._workbook = None
            cls._sheet = None

            cls._path = 'files'
            cls._dir_name = '{:%d.%m.%Y}'.format(datetime.today())
            cls._current_line = 1

        return cls.instance

    def create_book(self, name):
        self._file_name = name
        self._workbook = xlwt3.Workbook()

    def save_book(self):
        if not os.path.exists(self._path):
            os.makedirs(self._path)
        if not os.path.exists('{}//{}'.format(self._path, self._dir_name)):
            os.makedirs('{}//{}'.format(self._path, self._dir_name))
        self._workbook.save('{}//{}//{}.xls'.format(self._path, self._dir_name ,self._file_name))
        self._file_name = None
        self._workbook = None
        self._sheet = None

    def create_sheet(self, name):
        self._current_line = 1
        self._sheet = self._workbook.add_sheet(name)

    def add_header(self, *args):
        for index, arg in enumerate(args):
            self._sheet.write(0, index, arg)
            self._sheet.col(index).width = get_width(arg)

    def add_line(self, *args):
        for index, arg in enumerate(args):
            self._sheet.write(self._current_line, index, arg)
        self._current_line += 1


class WriterTestDrive:
    def __init__(self):
        w = Writer()
        w.create_book('test')
        w.create_sheet('Вкладка 1')
        w.add_header('1', '2', '3', '4')
        w.add_line('test1', 'test2', 'test3', 'test4')
        w.create_sheet('Вкладка 2')
        w.add_header('5', '6', '7', '8')
        w.add_line('test5', 'test6', 'test7', 'test8')
        w.save_book()

if __name__ == '__main__':
    w = WriterTestDrive()