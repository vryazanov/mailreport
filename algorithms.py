"""
Файл содержит алгоритмы, которые необходимы для формирования отчетов.
"""

__author__ = 'vryazanov'
__all__ = ['inaccessibility_alg', 'worktime', 'sec_to_workday', 'parse_excel', 'get_info']

from functools import reduce
from datetime import datetime, timedelta
from decimal import Decimal

def worktime(fromdate, todate):
    """
    Считает рабочее время между датами. Рабочее время с 11:00 до 19:00.
    Праздники не учитываются.
    """
    if not (isinstance(fromdate, datetime) and isinstance(todate, datetime)):
        print(fromdate, todate)
        raise TypeError

    daygenerator = [fromdate.date() + timedelta(x + 1) for x in range((todate.date() - fromdate.date()).days - 1)]
    total_seconds = sum(28800 for day in daygenerator if day.weekday() < 5)
    seconds = 0
    if fromdate.date() != todate.date():
        if fromdate.weekday() < 5:
            if 11 <= fromdate.hour < 19:
                seconds = 68400 - (fromdate.time().hour * 60 * 60 + fromdate.time().minute * 60 + fromdate.time().second)
            elif fromdate.hour < 11:
                seconds = 28800
            else:
                seconds = 0

            total_seconds += seconds

        if todate.weekday() < 5:
            if 11 <= todate.hour < 19:
                seconds = (todate.time().hour * 60 * 60 + todate.time().minute * 60 + todate.time().second) - 39600
            elif todate.hour >= 19:
                seconds = 28800
            else:
                seconds = 0

            total_seconds += seconds
    elif fromdate.weekday() < 5:
        if fromdate.hour >= 11 and todate.hour < 19:
            return int((todate - fromdate).total_seconds())

        if fromdate.hour >= 19 or todate.hour < 11:
            return 0

        return 68400 - (fromdate.time().hour * 60 * 60 + fromdate.time().minute * 60 + fromdate.time().second)
    else:
        return 0

    return total_seconds

def sec_to_workday(seconds, rnd='down'):
    """
    Из секунд в рабочие дни.
    """
    if seconds < 0:
        return 1

    hours = Decimal(float(seconds) / 3600.0)

    if hours % 8 == 0:
        return int(hours // 8)

    delta = 0.5 if rnd == 'up' else 0
    return int(Decimal(float(hours) / 8 + delta).quantize(Decimal(1), rounding='ROUND_HALF_UP'))

def timedelta_to_str(arg):
    s = arg.seconds + arg.days * 60 * 60 * 24
    hours = s // 3600
    arg = s - (hours * 3600)
    minutes = arg // 60
    seconds = arg - (minutes * 60)
    hours = str(hours)
    if len(hours) == 1:
        hours = '0' + hours
    minutes = str(minutes)
    if len(minutes) == 1:
        minutes = '0' + minutes
    seconds = str(seconds)
    if len(seconds) == 1:
        seconds = '0' + seconds
    return '{0}:{1}:{2}'.format(hours,minutes,seconds)


def inaccessibility_alg(lst, from_datetime, to_datetime):
    """
    Вычисление времени недоступности сервиса.
    Входные данные - список кортежей вида (флаг доступности, дата).
    """
    if not lst:
        return '00:00:00'

    if lst[0][-1] == 'Y':
        lst.insert(0, (from_datetime, 'N'))
    if lst[-1][-1] == 'N':
        lst.append((to_datetime, 'Y'))

    tdelta = [b[0]-a[0] for a, b in zip(lst[::2], lst[1::2])]

    return timedelta_to_str(reduce(lambda x, y: x+y, tdelta))

def parse_excel(filename):
    """
    Парсим из файла "МВ ответственные" почтовые адреса ФОИВов.
    """
    import xlrd
    rb = xlrd.open_workbook(filename)
    sheet = rb.sheet_by_index(0)

    dict_department = {}
    last_department = ''
    for row in range(2, sheet.nrows):
        department, mail, *args, mail2 = sheet.row_values(row)

        if not department:
            department = last_department
        else:
            last_department = department

        department = department.strip().split('.')[0]

        if department not in dict_department:
            dict_department[department]= []

        if mail:
            dict_department[department].append(mail.strip())

        if mail2:
            dict_department[department].append(mail2.strip())

    return dict_department

def get_info(lst=None):
    lst = list(reversed(lst))
    #print(lst)
    start = None #время решения/закрытия тикета
    end = None #время предоставления полной информации

    pending = 0 #время ожидания в секундах
    for x in lst:
        if x[-1] == 'Pending':
            pending += worktime(x[0], x[1] if x[1] else datetime.now())

    flag = False
    for x in lst:
        if x[-1] in ['Assigned', 'In Progress']:
            if not flag:
                flag = True
                end = x[1] if x[1] else datetime.now()
            start = x[0]
        else:
            if flag:
                break

    return (start, end, pending)


if __name__ == '__main__':
    #from datetime import datetime
    #to_date = datetime(2013, 10, 10)
    #from_date = datetime(2013, 10, 1)
    #print(to_date)
    #test1 = [(datetime(2013, 10, 10, 10, 30, 30), 'N'), (datetime(2013, 10, 10, 10, 40, 30), 'Y')]
    #test2 = [(datetime(2013, 10, 1, 0, 5, 0), 'Y'), (datetime(2013, 10, 9, 23, 58, 0), 'N')]
    #inaccessibility_alg(test2, from_date, to_date)
    #print(worktime(datetime(2013, 8, 15, 11, 0, 0),datetime(2013, 8, 15, 11, 1, 0)))
    #d = parse_excel('d:\\123.xls')
    print(sec_to_workday(180000))