"""
Реализация итератора и компоновщика.
Используется для удобного представления древовидной структуры ФОИВ/ИС/Сервис.
"""

__author__ = 'vryazanov'
__all__ = ['get_composite', 'Component']

from abc import ABCMeta, abstractmethod
from database import DataFactory

def get_composite():
    """
    Преобразует данные по сервисам/ИС ФОИВов из кортежа в дерево компоновщика.
    Возвращает объект типа Component.
    """
    main_component = Component('Главный узел', '/')
    df = DataFactory()
    lst = df.get_data('getOIV')

    for org_name, org_mnemonic, system_name, system_mnemonic, srv_name, srv_sid, srv_mnemonic in lst:
        oiv = Component(org_name, org_mnemonic)
        information_system = Component(system_name, system_mnemonic)
        service_sid = ComponentItem(srv_name, srv_mnemonic, srv_sid)

        #Проверка наличия ИС у ФОИВ
        if oiv not in main_component:
            main_component.add(oiv)

        if information_system not in main_component[oiv]:
            main_component[oiv].add(information_system)

        if service_sid not in main_component[oiv][information_system]:
            main_component[oiv][information_system].add(service_sid)

    return main_component

class InterfaceComponent(metaclass=ABCMeta):
    """
    Абстрактный класс паттерна компоновщик.
    """

    @abstractmethod
    def get_name(self): pass

    @abstractmethod
    def get_mnemonic(self): pass

    def add(self, component): pass

    def remove(self, component): pass

    def get_child(self, x): pass

class ComponentItem(InterfaceComponent):
    """
    Реализация листового узла.
    """
    def __init__(self, name, mnemonic, sid):
        self._name = name
        self._mnemonic = mnemonic
        self._sid = sid

    def __str__(self):
        return '[Листовой узел] Service name: {}. Service mnemonic: {}. Service SID: {}.'.format(self._name, self._mnemonic, self._sid)

    def get_name(self):
        return self._name

    def get_mnemonic(self):
        return self._mnemonic

    def get_sid(self):
        return self._sid


class Component(InterfaceComponent):
    """
    Реализация комбинационного узла.
    """
    def __init__(self, name, mnemonic):
        self.components = []
        self._name = name
        self._mnemonic = mnemonic
        self.current = 0

    def __str__(self):
        return '[Комбинационный узел] Name: {}. Mnemonic: {}'.format(self._name, self._mnemonic)

    def __iter__(self):
        return self

    def __len__(self):
        return len(self.components)

    def __getitem__(self, item):
        for component in self.components:
            if (component.get_name() == item.get_name()) and (component.get_mnemonic() == item.get_mnemonic()):
                return component
        raise KeyError

    def __next__(self):
        self.current += 1
        if len(self.components) >= self.current:
            return self.components[self.current-1]
        else:
            self.current = 0
            raise StopIteration

    def __contains__(self, item):
        assert isinstance(item, InterfaceComponent)

        for component in self.components:
            if (component.get_name() == item.get_name()) and (component.get_mnemonic() == item.get_mnemonic()):
                return True
        return False

    def get_mnemonic(self):
        return self._mnemonic

    def get_name(self):
        return self._name

    #Группа комбинационных методов
    def get_child(self, x):
        return self.components[x]

    def add(self, component):
        if component not in self.components:
            self.components.append(component)

    def remove(self, component):
        self.components.remove(component)

class ComponentTestDrive:
    def __init__(self):
        self.component = Component('SMEV','SMEV')
        #------------------------------------------
        foiv1 = Component('FOIV1', 'FOIV1')
        is1 = Component('IS1', 'IS01')
        service1 = ComponentItem('SRV_NAME', 'SRV', 'SID0000001')
        is1.add(service1)
        foiv1.add(is1)
        #------------------------------------------
        #------------------------------------------
        foiv2 = Component('FOIV2', 'FOIV2')
        is2 = Component('IS2', 'IS02')
        foiv2.add(is2)
        #------------------------------------------
        #------------------------------------------
        is3 = Component('IS3', 'IS03')
        service3 = ComponentItem('SRV_NAME2', 'SRV2', 'SID0000003')
        is3.add(service3)
        foiv1.add(is3)
        #------------------------------------------
        self.component.add(foiv1)
        self.component.add(foiv2)

        self._print()

    def _print(self):
        for component in self.component:
            print(component)
            for c in component:
                print('***', c)
                if isinstance(c, ComponentItem):
                    continue
                for c1 in c:
                    print('******', c1)

assert issubclass(ComponentItem, InterfaceComponent)
assert isinstance(ComponentItem(1, 2, 3), InterfaceComponent)
assert issubclass(Component, InterfaceComponent)
assert isinstance(Component(1, 2), InterfaceComponent)

if __name__ == '__main__':
    c = ComponentTestDrive()
    #c1 = get_composite()



