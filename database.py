"""
Файл, предоставляющий функции, для получения данных.
"""

__author__ = 'vryazanov'
__all__ = ['DataFactory', 'IteratorOIV']

import cx_Oracle

sql = {
    'inaccessibilityService': """
        SElECT
            status_changed,
            st.new_status
        FROM
            SREGISTRY.smev_services_stat_history st,
            SREGISTRY.find_service_view fs
        WHERE
            st.srv_sid=fs.srv_sid AND
            fs.node_mnemonic='p00smev' AND
            st.status_changed BETWEEN to_date('{from_date}', 'dd.mm.yyyy') AND to_date('{to_date}', 'dd.mm.yyyy') AND
            fs.hide_in_frontend='N' AND
            smev_node=1 AND
            st.srv_sid = '{sid}'
        ORDER BY fs.srv_sid, st.status_changed
    """,
    'getOIV': """
        SELECT
            do.org_name,
            do.org_mnemonic,
            dis.system_name,
            dis.system_mnemonic,
            ss.srv_name,
            ss1.srv_sid,
            ss.srv_mnemonic
        FROM
            siasmev.og_relation og,
            siasmev.operator o,
            sregistry.dic_information_systems dis,
            sregistry.dic_organizations do,
            sregistry.sr_service ss,
            sregistry.sr_service_version ssv,
            sregistry.smev_services ss1
        WHERE
            og.og_id = '1' AND
            og.user_id = o.user_id AND
            o.user_login = dis.system_mnemonic AND
            do.org_id = dis.org_id AND
            dis.system_id = ss.system_id AND
            ss.srv_id = ssv.srv_id AND
            ss1.version_id = ssv.version_id AND
            ss1.smev_node = 1
    """,
    'incomingRequests': """
        SELECT
            sum(pp.log_req_count)
        FROM
            PPORTAL.smevlb_log_aggr pp
        WHERE
            pp.LOG_REQ_TYPE = 'POST' AND
            log_sid = '{sid}' AND
            log_date BETWEEN to_date('{from_date}', 'dd.mm.yyyy') AND to_date('{to_date}', 'dd.mm.yyyy')
    """,
    'outboxRequests': """
        SELECT
            do.org_name,
            ss.srv_name,
            md.service_sid,
            count(*)
        FROM
            smevlogs.message_data md,
            siasmev.operator o,
            sregistry.dic_information_systems dis,
            sregistry.dic_organizations do,
            sregistry.sr_service ss,
            sregistry.sr_service_version ssv,
            sregistry.smev_services ss1
        WHERE
            md.log_type = 'Request' AND
            md.certserial = o.dscertificate_serial AND
            md.second_invoke_time BETWEEN to_date('{from_date}','dd.mm.yyyy') AND to_date('{to_date}','dd.mm.yyyy') AND
            o.user_login = '{mnemonic}' AND
            do.org_id = dis.org_id AND
            dis.system_id = ss.system_id AND
            ss.srv_id = ssv.srv_id AND
            ss1.version_id = ssv.version_id AND
            ss1.smev_node = 1 AND
            ss1.srv_sid = md.service_sid
        GROUP BY md.service_sid, do.org_name, ss.srv_name
    """,
    'incidentOIV': """
        SELECT
            HPD_HELP_DESK.Incident_Number , --
            fn_adjusted_date( HPD_HELP_DESK.Submit_Date),
            nvl( HPD_HELP_DESK.Description,'Not Available'),
            HPD_HELP_DESK.Categorization_Tier_3,
            fn_adjusted_date( HPD_HELP_DESK.Last_Modified_Date),
            Detailed_Description,
            Table__292.status
        FROM
            HPD_WORKLOG RIGHT OUTER JOIN (
                SELECT HPD.*
                FROM   HPD_HELP_DESK  HPD
                WHERE EXISTS(
                    SELECT DISTINCT CTM1.COMPANY
                    FROM CTM_PEOPLE_PERMISSION_GROUPS CTM1
                    WHERE UPPER(REMEDY_LOGIN_ID) = UPPER('mpolyakova')
                    AND CTM1.COMPANY IS NOT NULL
                    AND ( CTM1.COMPANY = HPD.COMPANY
                    OR CTM1.COMPANY = HPD.CONTACT_COMPANY
                    OR CTM1.COMPANY = HPD.Direct_Contact_Company
                    OR CTM1.COMPANY = HPD.Assigned_Support_Company
                    OR CTM1.COMPANY = HPD.Owner_Support_Company)
                    UNION
                    SELECT DISTINCT CTM2.COMPANY
                    FROM CTM_PEOPLE_ORGANIZATION CTM2
                    WHERE
                    (SELECT 1
                    FROM CTM_PEOPLE_PERMISSION_GROUPS
                    WHERE UPPER(REMEDY_LOGIN_ID) = UPPER('mpolyakova')
                    AND PERMISSION_GROUP = 'Unrestricted Access') = 1
                    AND ( CTM2.COMPANY = HPD.COMPANY
                    OR CTM2.COMPANY = HPD.CONTACT_COMPANY
                    OR CTM2.COMPANY = HPD.Direct_Contact_Company
                    OR CTM2.COMPANY = HPD.Assigned_Support_Company
                    OR CTM2.COMPANY = HPD.Owner_Support_Company)
                )
            )  HPD_HELP_DESK ON (HPD_WORKLOG.INCIDENT_NUMBER=HPD_HELP_DESK.INCIDENT_NUMBER)
                LEFT OUTER JOIN (
                    SELECT DISTINCT ENUMID,ENUMVALUE AS STATUS
                    FROM  ANA_ALL_ENUM_VALUES
                    WHERE RPT_LOCALEFLAG=1
                    AND RESOLVED_SCHEMANAME='HPD:Help Desk'
                    AND FIELDID =  7
                )  Table__292 ON (HPD_HELP_DESK.STATUS=Table__292.ENUMID)
                    LEFT OUTER JOIN (
                        SELECT DISTINCT ENUMID,ENUMVALUE AS STATUS_REASON
                        FROM ANA_ALL_ENUM_VALUES
                        WHERE RPT_LOCALEFLAG=1
                        AND RESOLVED_SCHEMANAME='HPD:Help Desk'
                        AND FIELDID = 1000000150
                )  Table__296 ON (HPD_HELP_DESK.STATUS_REASON=Table__296.ENUMID)
                    LEFT OUTER JOIN (
                        SELECT DISTINCT ENUMID,ENUMVALUE AS SLM_STATUS
                        FROM ANA_ALL_ENUM_VALUES
                        WHERE RPT_LOCALEFLAG=1
                        AND RESOLVED_SCHEMANAME=  'HPD:Help Desk'
                        AND FIELDID = 1000003009
                )  SLM_STATUS ON (HPD_HELP_DESK.SLM_STATUS=SLM_STATUS.ENUMID)
                    LEFT OUTER JOIN (
                        SELECT fev.enumid, fev.value incident_type
                        FROM  FIELD_ENUM_VALUES fev, arschema arsc
                        WHERE fev.schemaid = arsc.schemaid
                        AND arsc.name='HPD:Help Desk__o'
                        AND fev.FIELDID =  801060004
                )  HPD_INCIDENT_TYPES ON (HPD_INCIDENT_TYPES.ENUMID=HPD_HELP_DESK.INCIDENT_TYPE)
                    LEFT OUTER JOIN (
                        SELECT ENUMID,ENUMVALUE AS commercial_organizations_ddl
                        FROM ANA_ALL_ENUM_VALUES
                        WHERE RPT_LOCALEFLAG=0
                        AND RESOLVED_SCHEMANAME='HPD:Help Desk'
                        AND FIELDID = 536870987
                )  Table__1873 ON (HPD_HELP_DESK.COMMERCIAL_ORGANIZATIONS_DDL=Table__1873.ENUMID)
        WHERE
            (
            HPD_HELP_DESK.ServiceCI  LIKE  '%СМЭВ%'
            AND
            HPD_HELP_DESK.Owner_Group   IN  ( 'Проект  СМЭВ'  )
            AND
            Table__292.status IN {status}
            AND
            HPD_HELP_DESK.CONTACT_IN_THE_DEPARTMENT IN {mails}
            )
    """,
    'completeInformation': """
		SELECT
		  HPD_HELP_DESK.Incident_Number ,
		  HPD_HCB_ASSIGNMENTHISTORY_JOIN.P_ASSIGNEDGROUP,
		  HPD_HCB_ASSIGNMENTHISTORY_JOIN.C_ASSIGNEDGROUP,
		  fn_adjusted_date(HPD_HCB_ASSIGNMENTHISTORY_JOIN.C_STARTTIMESTAMP),
		  fn_adjusted_date(HPD_HCB_ASSIGNMENTHISTORY_JOIN.C_ENDTIMESTAMP),
		  HPD_ASH_STATUS_C.STATUS
		FROM
		  (

		SELECT DISTINCT ENUMID,ENUMVALUE AS STATUS
		FROM  ANA_ALL_ENUM_VALUES
		WHERE RPT_LOCALEFLAG=1
		AND RESOLVED_SCHEMANAME='HPD:Help Desk'
		AND FIELDID =  7


		  )  Table__292 RIGHT OUTER JOIN (
		  SELECT HPD.*
		FROM   HPD_HELP_DESK  HPD
		WHERE EXISTS(
		SELECT DISTINCT CTM1.COMPANY
			 FROM CTM_PEOPLE_PERMISSION_GROUPS CTM1
			 WHERE UPPER(REMEDY_LOGIN_ID) = UPPER('mpolyakova')
			   AND CTM1.COMPANY IS NOT NULL
			   AND ( CTM1.COMPANY = HPD.COMPANY
			   OR CTM1.COMPANY = HPD.CONTACT_COMPANY
			   OR CTM1.COMPANY = HPD.Direct_Contact_Company
			   OR CTM1.COMPANY = HPD.Assigned_Support_Company
			   OR CTM1.COMPANY = HPD.Owner_Support_Company) UNION SELECT DISTINCT CTM2.COMPANY
			 FROM CTM_PEOPLE_ORGANIZATION CTM2
			 WHERE
				 (SELECT 1
				  FROM CTM_PEOPLE_PERMISSION_GROUPS
				  WHERE UPPER(REMEDY_LOGIN_ID) = UPPER('mpolyakova')
					AND PERMISSION_GROUP = 'Unrestricted Access') = 1
			   AND ( CTM2.COMPANY = HPD.COMPANY
			   OR CTM2.COMPANY = HPD.CONTACT_COMPANY
			   OR CTM2.COMPANY = HPD.Direct_Contact_Company
			   OR CTM2.COMPANY = HPD.Assigned_Support_Company
			   OR CTM2.COMPANY = HPD.Owner_Support_Company)
		)


		  )  HPD_HELP_DESK ON (HPD_HELP_DESK.STATUS=Table__292.ENUMID)
		   LEFT OUTER JOIN HPD_HCB_ASSIGNMENTHISTORY_JOIN ON (HPD_HCB_ASSIGNMENTHISTORY_JOIN.INCIDENT_NUMBER=HPD_HELP_DESK.INCIDENT_NUMBER)
		   LEFT OUTER JOIN (
		  SELECT DISTINCT ENUMID,ENUMVALUE AS STATUS
		FROM  ANA_ALL_ENUM_VALUES
		WHERE RPT_LOCALEFLAG=1
		AND RESOLVED_SCHEMANAME='HPD:Help Desk'
		AND FIELDID =  7
		  )  HPD_ASH_STATUS_C ON (HPD_ASH_STATUS_C.ENUMID=HPD_HCB_ASSIGNMENTHISTORY_JOIN.C_INCIDENTSTATUS)
		   LEFT OUTER JOIN (
		  SELECT ENUMID,ENUMVALUE AS commercial_organizations_ddl
		FROM ANA_ALL_ENUM_VALUES
		WHERE RPT_LOCALEFLAG=0
		AND RESOLVED_SCHEMANAME='HPD:Help Desk'
		AND FIELDID = 536870987


		  )  Table__1873 ON (HPD_HELP_DESK.COMMERCIAL_ORGANIZATIONS_DDL=Table__1873.ENUMID)

		WHERE
		  (
		   HPD_HELP_DESK.ServiceCI  LIKE  '%СМЭВ%'
		   AND
		   HPD_HELP_DESK.Owner_Group   IN  ( 'Проект  СМЭВ'  )
		   AND
		   HPD_HELP_DESK.CONTACT_IN_THE_DEPARTMENT IN {mails}
		   AND
		   HPD_HELP_DESK.Incident_Number   NOT IN  ( 'INC000000241470','INC000000232684','INC000000283093'  )
		  ) ORDER BY HPD_HELP_DESK.Incident_Number, fn_adjusted_date(HPD_HCB_ASSIGNMENTHISTORY_JOIN.C_STARTTIMESTAMP)
    """
}

class DataFactory:
    """
    Класс осуществляет выгрузку данных из БД.
    """
    def __init__(self):
        import os
        os.environ['NLS_LANG'] = '.UTF8'
        self.oracle_db = {
            'federal':['172.20.4.26', 1521, 'p00smev.rt.ru', 'ATC_NZHEGANOVA', 'Nakle11a'],
            'skuf':['10.62.9.172', 1521, 'p00skuf', 'ATC_VRYAZANOV', 'BUF6fsS#']
        }

    def get_data(self, str, format=None):
        assert str in sql, 'SQL not found'

        if str in ['inaccessibilityService', 'getOIV', 'incomingRequests', 'outboxRequests']:
            tns = cx_Oracle.makedsn(self.oracle_db['federal'][0], self.oracle_db['federal'][1], service_name = self.oracle_db['federal'][2])
            connection = cx_Oracle.connect(self.oracle_db['federal'][3], self.oracle_db['federal'][4], tns)
        elif str in ['incidentPending', 'completeInformation', 'incidentOIV']:
            tns = cx_Oracle.makedsn(self.oracle_db['skuf'][0], self.oracle_db['skuf'][1], sid = self.oracle_db['skuf'][2])
            connection = cx_Oracle.connect(self.oracle_db['skuf'][3], self.oracle_db['skuf'][4], tns)

        cursor = connection.cursor()
        cursor.arraysize = 500
        #print(sql[str].format(**format) if format else sql[str])
        cursor.execute(sql[str].format(**format) if format else sql[str])
        return  cursor.fetchall()

if __name__ == '__main__':
    import settings
    f = DataFactory()
    print(f.get_data('incidentPending', format={'mails': str(tuple(['smev@raiffeisen.ru', 'None']))}))
