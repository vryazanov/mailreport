__author__ = 'vryazanov'

import logging
import logging.config
import traceback
import sys
import os
from composite import get_composite
from excel import Writer
from mail import send_mail
from model import *
from datetime import datetime
import settings

class DatedRotatingFileHandler(logging.FileHandler):
    def __init__(self, path):
        os.mkdir(path)
        super(DatedRotatingFileHandler, self).__init__('{}//{:%d.%m.%Y}'.format(path, datetime.today()), 'a')

logging.DatedRotatingFileHandler = DatedRotatingFileHandler

class Main:
    def __init__(self):
        logging.config.fileConfig('logging.conf')
        self.log = logging.getLogger('main')

        try:
            self._lst = get_composite()
        except:
            self.log.critical(traceback.format_exc(10))
            sys.exit(1)

        self.reports = [] #список моделей отчетов

    def start(self):
        w = Writer()
        self.log.info('Всего ОИВов: {}'.format(len(self._lst)))
        for index, organization in enumerate(self._lst):
            org_name = organization.get_mnemonic()
            self.log.info('Формируем отчет для <{}>'.format(org_name))
            w.create_book(org_name)
            for report in self.reports:
                try:
                    r = report()
                    r.generate(organization)
                except:
                    self.log.critical(traceback.format_exc(10))
            w.save_book()
            self.log.info('[{}/{}] Отчет для {} сформирован.'.format(index+1, len(self._lst), org_name))
            try:
                self.send_mail(organization)
            except:
                self.log.critical(traceback.format_exc(10))

    def send_mail(self, organization):
        file = '{}\\{}\\{}.xls'.format('files', '{:%d.%m.%Y}'.format(datetime.today()), organization.get_mnemonic())
        subject = 'Еженедельный отчет [{}]'.format(organization.get_mnemonic())
        text =  'Ведомство: {}\n' \
                'Список почтовых ящиков: {}\n'.format(organization.get_name(), ', '.join(settings.MAILS[organization.get_name()]))
        send_mail('smev-support@yandex.ru', ['vryazanov@at-consulting.ru'], subject, text, [file])

if __name__ == '__main__':
    m = Main()
    m.reports.append(OIVIncident)
    m.reports.append(IncidentPending)
    m.reports.append(OutboxRequestModel)
    m.reports.append(InaccessibilityModel)
    m.reports.append(IncomingRequestModel)
    m.start()

